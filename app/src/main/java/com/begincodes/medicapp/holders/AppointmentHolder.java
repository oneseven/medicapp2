package com.begincodes.medicapp.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.begincodes.medicapp.R;

/**
 * Created by manuelguarniz on 11/12/16.
 */
public class AppointmentHolder extends RecyclerView.ViewHolder {
    public TextView specialties;
    public TextView datePrograming;
    public TextView hours;

    public AppointmentHolder(View itemView) {
        super(itemView);
        specialties = (TextView) itemView.findViewById(R.id.tvDetailSpecialties);
        datePrograming = (TextView) itemView.findViewById(R.id.tvDetailDatePrograming);
        hours = (TextView) itemView.findViewById(R.id.tvDetailHours);
    }
}
