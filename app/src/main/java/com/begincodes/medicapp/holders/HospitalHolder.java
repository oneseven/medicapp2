package com.begincodes.medicapp.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.begincodes.medicapp.R;

/**
 * Created by manuelguarniz on 23/11/16.
 */
public class HospitalHolder extends RecyclerView.ViewHolder {
    public ImageView image;
    public TextView name;
    public TextView address;
    public TextView description;
    public Button bRegister;
    public Button bCita;

    public HospitalHolder(View itemView) {
        super(itemView);
        image = (ImageView) itemView.findViewById(R.id.imageHospital);
        name = (TextView) itemView.findViewById(R.id.tvNameHospital);
        address = (TextView) itemView.findViewById(R.id.tvAdressHospital);
        description = (TextView) itemView.findViewById(R.id.tvDescriptionHospital);

        bCita = (Button) itemView.findViewById(R.id.bCita);
    }
}
