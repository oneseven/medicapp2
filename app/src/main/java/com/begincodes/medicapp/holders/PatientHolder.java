package com.begincodes.medicapp.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.begincodes.medicapp.R;

/**
 * Created by manuelguarniz on 29/11/16.
 */
public class PatientHolder extends RecyclerView.ViewHolder{

    public ImageView imagePatient;
    public TextView tvNamePatient;
    public TextView tvLastNamePatient;
    public TextView tvAgePatient;
    public TextView tvDniPatient;
    public TextView tvOriginPatient;

    public PatientHolder(View view) {
        super(view);
        imagePatient = (ImageView) view.findViewById(R.id.imagePatient);
        tvNamePatient = (TextView) view.findViewById(R.id.tvNamePatient);
        tvLastNamePatient = (TextView) view.findViewById(R.id.tvLastNamePatient);
        tvAgePatient = (TextView) view.findViewById(R.id.tvAgePatient);
        tvDniPatient = (TextView) view.findViewById(R.id.tvDniPatient);
        tvOriginPatient = (TextView) view.findViewById(R.id.tvOriginPatient);
    }
}
