package com.begincodes.medicapp.utils;

/**
 * Created by manuelguarniz on 30/11/16.
 */
public class FirebaseConstants {

    
    public FirebaseConstants() {
    }

    public static final String CHILD_PATIENT = "pacientes";
    public static final String CHILD_HOSPITAL = "hospitales";
    public static final String CHILD_MEDICALS = "medicals";
    public static final String CHILD_SPECIALTIES = "especialties";
    public static final String CHILD_APPOINTMENT = "citas";

    public static final String URL_PHOTO[] = new String[] {
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2F100417.jpg?alt=media&token=71f42a54-3b20-416a-8b46-20be3516c9b4",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2F1236860171_1.jpg?alt=media&token=b5d29e74-5b64-470e-bc49-44d9d80cd35c",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2F3b48cafe8dfdf028a21c6d89655b21d5.jpg?alt=media&token=cafb77be-17a8-4b18-81b5-933571517011",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2FCatalina_Webphoto.jpg?alt=media&token=9cef0b69-3322-4f4e-8d4b-e30f02bf04c0",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2Fcomo-leer-el-lenguaje-corporal.jpg?alt=media&token=092cd1e5-ebe3-453f-ba27-4e973d90e1b2",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2FConfiesa-matado-personas-detenido-encadenar_969513816_116678483_667x489.jpg?alt=media&token=360ff51d-a0be-4143-9faf-330d87eb671c",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2Fempleado-retirado.jpg?alt=media&token=773c34cc-bf8a-4573-93b7-2cb4ec01fe5e",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2Fimpresion-cara.jpg?alt=media&token=21bc1b1e-3edf-46d6-b521-6a9be90a0368",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2Fpersonas%20(1).jpg?alt=media&token=f07cb8f1-277e-4932-8e67-64ded04f9557",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2Fpersonas.jpg?alt=media&token=09dda85e-1b16-4ef1-b404-8508ac09d22e",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2FRoberto-P%C3%A9rez-Baeza.jpg?alt=media&token=764af4e6-efde-439f-88c8-0f7796417a4b",
            "https://firebasestorage.googleapis.com/v0/b/medicapp-8b49d.appspot.com/o/profile%2Ftumbes-tres-personas-armada_Gje1BBl-jpg_604x0.jpg?alt=media&token=d1f40697-31a6-4e37-8d5b-f786f8eb79d8"
    };
    public static final String LIST_SPECIALTIES[] = new String[] {
            "medicina general",
            "pediatria",
            "dental",
            "obsstetricia",
            "ginecologia",
            "psicologia",
            "otorrinolaringologia"
    };
}
