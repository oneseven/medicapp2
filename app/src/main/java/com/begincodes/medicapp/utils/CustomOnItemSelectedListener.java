package com.begincodes.medicapp.utils;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.begincodes.medicapp.R;

/**
 * Created by manuelguarniz on 11/12/16.
 */
public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        switch (view.getId()){
            case R.id.spListSpecialties:
                Toast.makeText(parent.getContext(),
                        "OnItemSelectedListener : " + parent.getItemAtPosition(pos).toString(),
                        Toast.LENGTH_SHORT).show();
                break;
            default: break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
