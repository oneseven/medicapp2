package com.begincodes.medicapp.models;

/**
 * Created by manuelguarniz on 05/12/16.
 */
public class Medical extends Person{
    private String specialty;

    public Medical(String lastName, String dni, int age, String status, String name, String origin, String photo_profile, String birthdate, String specialty) {
        super(lastName, dni, age, status, name, origin, photo_profile, birthdate);
        this.specialty = specialty;
    }

    public Medical() {
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }
}
