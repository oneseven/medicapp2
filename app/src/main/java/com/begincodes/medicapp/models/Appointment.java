package com.begincodes.medicapp.models;


/**
 * Created by manuelguarniz on 11/12/16.
 */
public class Appointment {
    private String programmingDate;
    private String hours;
    private String specialty;
    private String patient;

    public Appointment() {

    }

    public Appointment(String programmingDate, String hours, String specialty, String patient) {
        this.programmingDate = programmingDate;
        this.hours = hours;
        this.specialty = specialty;
        this.patient = patient;
    }

    public String getProgrammingDate() {
        return programmingDate;
    }

    public void setProgrammingDate(String programmingDate) {
        this.programmingDate = programmingDate;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }
}
