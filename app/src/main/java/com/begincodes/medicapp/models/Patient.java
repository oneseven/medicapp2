package com.begincodes.medicapp.models;

import java.util.Date;

/**
 * Created by manuelguarniz on 27/11/16.
 */
public class Patient extends Person {
    private String medical_history;

    public Patient(String lastName, String dni, int age, String status, String name, String origin, String photo_profile, String birthdate, String medical_history) {
        super(lastName, dni, age, status, name, origin, photo_profile, birthdate);
        this.medical_history = medical_history;
    }

    public Patient() {
    }

    public String getMedical_history() {
        return medical_history;
    }

    public void setMedical_history(String medical_history) {
        this.medical_history = medical_history;
    }
}
