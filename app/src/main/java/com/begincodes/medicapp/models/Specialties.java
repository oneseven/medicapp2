package com.begincodes.medicapp.models;

import java.util.Map;

/**
 * Created by manuelguarniz on 05/12/16.
 */
public class Specialties {
    private String specialty;
    private Map<String, Boolean> medicals;
    private String status;

    public Specialties(String specialty, Map<String, Boolean> medicals, String status) {
        this.specialty = specialty;
        this.medicals = medicals;
        this.status = status;
    }
    public Specialties(){
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public Map<String, Boolean> getMedicals() {
        return medicals;
    }

    public void setMedicals(Map<String, Boolean> medicals) {
        this.medicals = medicals;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        status = status;
    }
}
