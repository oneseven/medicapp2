package com.begincodes.medicapp.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by manuelguarniz on 23/11/16.
 */
@IgnoreExtraProperties
public class Hospital {
    private String company;
    private String address;
    private String photo;
    private String description;

    public Hospital() {
    }

    public Hospital(String company, String address, String photo, String description) {
        this.company = company;
        this.address = address;
        this.photo = photo;
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

