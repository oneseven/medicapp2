package com.begincodes.medicapp.activitys;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.begincodes.medicapp.R;
import com.begincodes.medicapp.adapters.SpecialtiesAdapter;
import com.begincodes.medicapp.models.Appointment;
import com.begincodes.medicapp.models.Medical;
import com.begincodes.medicapp.models.Patient;
import com.begincodes.medicapp.models.Specialties;
import com.begincodes.medicapp.utils.CustomOnItemSelectedListener;
import com.begincodes.medicapp.utils.FirebaseConstants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppointmentActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    public static final String EXTRA_POST_KEY = "post_key";
    public static final String EXTRA_POST_KEY_BIRTH_DATE = "post_key_birth_date";
    private static final int DIALOG_ID = 0;

    private String dni;

    private String mPostKey;
    private String mPostKeyBirth;
    private static FirebaseDatabase database;
    private static DatabaseReference mRoot;
    private DatabaseReference mRefMedicals, mRefSpecialties, mRefPatient, mRefAppointment;

    private EditText etDatePrograming;
    private List<String> list = new ArrayList<>();
    private List<String> data = new ArrayList<>();
    private List<String> listTurno = new ArrayList<>();
    private Spinner spListSpecialties, spListTurno;

    private ImageView imageViewPatient;
    private TextView tvDetailPatient;
    private Button bInsertMedical;

    String keyMedicals;
    String keySpecialties;
    long num;
    int day;
    int month;
    int year;
    int var;
    int specialty;
    int i = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);

        database = FirebaseDatabase.getInstance();
        mRoot = database.getReference();

        mPostKey = getIntent().getStringExtra(EXTRA_POST_KEY);
        mPostKeyBirth = getIntent().getStringExtra(EXTRA_POST_KEY_BIRTH_DATE);
        if (mPostKey == null && mPostKeyBirth == null) {
            throw new IllegalArgumentException("Falta enviar parametro EXTRA_POST_KEY");
        }

        etDatePrograming = (EditText) findViewById(R.id.etDatePrograming);
        imageViewPatient = (ImageView) findViewById(R.id.imageViewPatient);
        tvDetailPatient = (TextView) findViewById(R.id.tvDetailPatient);
        spListSpecialties = (Spinner) findViewById(R.id.spListSpecialties);
        spListTurno = (Spinner) findViewById(R.id.spListTurno);

        bInsertMedical = (Button) findViewById(R.id.bInsertMedical);
        bInsertMedical.setOnClickListener(this);

        final Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        //Inicializar Referencias para Firebase
        initReferences();
        //Muestra detalles de los pacientes
        showDetailPatient();
        //Ver dialog del calendario
        onShowDialogCalendar();
        //Genera datos para el Spinner -> spListSpecialties
        generateData();

        SpecialtiesAdapter adapter = new SpecialtiesAdapter(list, AppointmentActivity.this);
        spListSpecialties.setAdapter(adapter);
        spListSpecialties.setDropDownVerticalOffset(100);
        spListSpecialties.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        SpecialtiesAdapter adapterTurno = new SpecialtiesAdapter(listTurno, AppointmentActivity.this);
        spListTurno.setAdapter(adapterTurno);
        spListSpecialties.setDropDownVerticalOffset(100);
        spListSpecialties.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    private void generateData() {
        mRefSpecialties.keepSynced(true);
        mRefSpecialties.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Specialties specialties = child.getValue(Specialties.class);
                    i++;
                    data.add(specialties.getSpecialty());
                    Log.i(TAG, "data -> "+ data.get(i));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        if (data.isEmpty()){
            data.add("Medicina General");
            data.add("Pediatria");
            data.add("Dental");
            data.add("Obsstetricia");
            data.add("Ginecologia");
            data.add("Psicologia");
            data.add("Otorrinolaringologia");
        }

        list.addAll(data);
        listTurno.add("Mañana");
        listTurno.add("Tarde");
    }

    private void initReferences() {
        mRefMedicals = mRoot.child(FirebaseConstants.CHILD_MEDICALS);
        mRefSpecialties = mRoot.child(FirebaseConstants.CHILD_SPECIALTIES);
        mRefPatient = mRoot.child(FirebaseConstants.CHILD_PATIENT);
        mRefAppointment = mRoot.child(FirebaseConstants.CHILD_APPOINTMENT);
    }

    private void showDetailPatient() {
        mRefPatient.child(mPostKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Patient model = dataSnapshot.getValue(Patient.class);
                Glide.with(getApplicationContext())
                        .load(model.getPhoto_profile())
                        .centerCrop()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageViewPatient);
                tvDetailPatient.setText(model.getName() + " " + model.getLastName());
                dni = model.getDni();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void onShowDialogCalendar() {
        etDatePrograming = (EditText) findViewById(R.id.etDatePrograming);
        etDatePrograming.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    showDialog(DIALOG_ID);
                } else {
                    dismissDialog(DIALOG_ID);
                }
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_ID) {
            return new DatePickerDialog(this, dPickerDialog, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener dPickerDialog = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int _year, int _month, int _day) {
            day = _day;
            month = _month;
            year = _year;
            String date = day + "/" + (month + 1) + "/" + year;
            etDatePrograming.setText(date);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bInsertMedical:
                //insertDataMedicals();
                Toast.makeText(getApplicationContext(), "Cita registrada", Toast.LENGTH_LONG).show();

                insertAppointmentDay(mPostKey);

                Intent intent = new Intent(getApplicationContext()  , SelectorActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(SelectorActivity.EXTRA_POST_DNI, dni);
                intent.putExtra(SelectorActivity.EXTRA_POST_KEY_BIRTH_DATE, backBirthDatePassword(mPostKeyBirth));
                getApplication().startActivity(intent);

                break;
            default:
                break;
        }
    }

    private void insertAppointmentDay(String key) {
        Appointment appointment = new Appointment();
        appointment.setProgrammingDate(etDatePrograming.getText().toString());
        if (listTurno.get(0).equalsIgnoreCase(listTurno.get(spListTurno.getSelectedItemPosition()))){
            appointment.setHours("08:00");
        } else {
            appointment.setHours("13:00");
        }
        appointment.setSpecialty(list.get(spListSpecialties.getSelectedItemPosition()));
        appointment.setPatient(key);

        Log.i(TAG, "select 1 -> " + spListSpecialties.getSelectedItemPosition());
        Log.i(TAG, "hours 1 -> " + appointment.getHours());
        Log.i(TAG, "date -> " + appointment.getProgrammingDate());
        Log.i(TAG, "especialidad -> " + appointment.getSpecialty());
        Log.i(TAG, "especialidad -> " + appointment.getPatient());

        String keyAppointment = mRefAppointment.push().getKey();
        mRefAppointment.child(keyAppointment).setValue(appointment);
    }

    private String backBirthDatePassword(final String date) {
        String[] partDate = date.split("/");
        String password = partDate[0] + partDate[1] + partDate[2];
        return password;
    }

    private void changesVars() {
        keyMedicals = mRefMedicals.push().getKey();
        keySpecialties = mRefSpecialties.push().getKey();

        num = (long) (Math.random() * 99999999 + 10000000);
        day = (int) (Math.random() * 30 + 1);
        month = (int) (Math.random() * 12 + 1);
        year = (int) (Math.random() * (2010 - 1970 + 1) + 1970);
        var = (int) (Math.random() * 12 + 0);
        specialty = (int) (Math.random() * 7 + 0);
    }

    private void insertDataMedicals() {

        changesVars();

        Medical medical = new Medical();
        medical.setLastName("apeDoc01" + num);
        medical.setDni(String.valueOf(num));
        medical.setAge(32);
        medical.setStatus("active");
        medical.setName("nameDoc");
        medical.setOrigin("trujillo");
        medical.setBirthdate(day + "/" + month + "/" + year);
        medical.setPhoto_profile(FirebaseConstants.URL_PHOTO[var]);
        medical.setSpecialty(FirebaseConstants.LIST_SPECIALTIES[specialty]);

        insertSpecialties();

        mRefMedicals.child(keyMedicals).setValue(medical);

    }

    private void insertSpecialties() {
        Specialties specialties = new Specialties();
        specialties.setSpecialty(FirebaseConstants.LIST_SPECIALTIES[specialty]);
        specialties.setStatus("active");
        Map<String, Boolean> map = new HashMap<String, Boolean>();
        map.put(keyMedicals, true);
        specialties.setMedicals(map);
        mRefSpecialties.child(keySpecialties).setValue(specialties);

    }
}
