package com.begincodes.medicapp.activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.begincodes.medicapp.R;
import com.begincodes.medicapp.holders.HospitalHolder;
import com.begincodes.medicapp.models.Hospital;
import com.begincodes.medicapp.utils.FirebaseConstants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private FirebaseDatabase database;
    private DatabaseReference mRoot;
    private DatabaseReference mRef;

    private RecyclerView recycler;
    private FirebaseRecyclerAdapter<Hospital, HospitalHolder> adapter;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.title);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        database = FirebaseDatabase.getInstance();
        mRoot = database.getReference();

        progressDialog = new ProgressDialog(this);

        recycler = (RecyclerView) findViewById(R.id.listHospital);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onStart() {
        super.onStart();
        mRef = mRoot.child(FirebaseConstants.CHILD_HOSPITAL);

        progressDialog.setMessage("Conectando...");
        progressDialog.show();

        showDataHospital();
    }

    private void showDataHospital() {

        mRef.keepSynced(true);
        adapter = new FirebaseRecyclerAdapter<Hospital, HospitalHolder>(Hospital.class,R.layout.card_item_hospital,HospitalHolder.class,mRef) {
            @Override
            protected void populateViewHolder(HospitalHolder viewHolder, Hospital model, int position) {
                Glide.with(getApplicationContext())
                        .load(model.getPhoto())
                        .centerCrop()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.image);

                viewHolder.name.setText(String.valueOf(model.getCompany()));
                viewHolder.address.setText(String.valueOf(model.getAddress()));
                viewHolder.description.setText(String.valueOf(model.getDescription()));

                progressDialog.dismiss();

                viewHolder.bCita.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), SelectorActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });
            }
        };
        recycler.setAdapter(adapter);

    }

}
