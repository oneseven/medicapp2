package com.begincodes.medicapp.activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.begincodes.medicapp.R;
import com.begincodes.medicapp.adapters.PendingAppointmentAdapter;
import com.begincodes.medicapp.holders.AppointmentHolder;
import com.begincodes.medicapp.models.Appointment;
import com.begincodes.medicapp.models.Patient;
import com.begincodes.medicapp.utils.FirebaseConstants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SelectorActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    public static final String EXTRA_POST_DNI = "post_key";
    public static final String EXTRA_POST_KEY_BIRTH_DATE = "post_key_birth_date";

    private FirebaseDatabase database;
    private DatabaseReference mRoot, mRef, mRefAppointment;

    private String mPostKey;
    private String mPostKeyBirth;

    private Button bSearch;
    private Button bNewSearch;
    private EditText etDni;
    private EditText etBirthDate;
    private View layoutCardID;
    private View layoutPendingAppointment;

    ///////////////////////////////////////////////////////////////////////////////////
    private ImageView imagePatient;
    private TextView tvNamePatient;
    private TextView tvLastNamePatient;
    private TextView tvAgePatient;
    private TextView tvDniPatient;
    private TextView tvOriginPatient;
    private ImageButton bImagePatient;
    private Button bRequestAppointment;

    private String dni;
    private String bithDate;
    private boolean verifyShowButtonOrList = false;

    private ProgressDialog progressDialog;

    //////////////////////////////////////////////

    private RecyclerView recyclePendingAppointment;
    private PendingAppointmentAdapter adapterAppointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selector);

        database = FirebaseDatabase.getInstance();
        mRoot = database.getReference();

        initReferences();

        etDni = (EditText) findViewById(R.id.etDni);
        etBirthDate = (EditText) findViewById(R.id.etBirthDate);

        layoutCardID = findViewById(R.id.layoutCardID);
        layoutPendingAppointment = findViewById(R.id.layoutPendingAppointment);

        progressDialog = new ProgressDialog(this);

        imagePatient = (ImageView) findViewById(R.id.imagePatient);
        tvNamePatient = (TextView) findViewById(R.id.tvNamePatient);
        tvLastNamePatient = (TextView) findViewById(R.id.tvLastNamePatient);
        tvAgePatient = (TextView) findViewById(R.id.tvAgePatient);
        tvDniPatient = (TextView) findViewById(R.id.tvDniPatient);
        tvOriginPatient = (TextView) findViewById(R.id.tvOriginPatient);

        //List pending appointment
        recyclePendingAppointment = (RecyclerView) findViewById(R.id.recyclePendingAppointment);
        adapterAppointment = new PendingAppointmentAdapter(this);
        recyclePendingAppointment.setAdapter(adapterAppointment);
        recyclePendingAppointment.setHasFixedSize(true);
        recyclePendingAppointment.setLayoutManager(new LinearLayoutManager(this));


        bSearch = (Button) findViewById(R.id.bSearch);
        //bNewSearch = (Button) findViewById(R.id.bNewSearch);

        bSearch.setOnClickListener(this);
        //bNewSearch.setOnClickListener(this);

        mPostKey = getIntent().getStringExtra(EXTRA_POST_DNI);
        mPostKeyBirth = getIntent().getStringExtra(EXTRA_POST_KEY_BIRTH_DATE);
        if (mPostKey == null && mPostKeyBirth == null) {
            verifyShowButtonOrList= true;
        } else {
            verifyShowButtonOrList = false;
            progressDialog.setMessage("Conectando...");
            progressDialog.show();

            searchPatients(mPostKey, mPostKeyBirth);
        }

        Log.i(TAG, "view data show -> "+ mPostKey+ " -> "+ mPostKeyBirth);

    }

    @Override
    protected void onStart() {
        super.onStart();
        layoutCardID.setVisibility(View.GONE);
    }

    private void initReferences() {
        mRef = mRoot.child(FirebaseConstants.CHILD_PATIENT);
        mRefAppointment = mRoot.child(FirebaseConstants.CHILD_APPOINTMENT);
    }

    private void insertData() {
        mRef = mRoot.child(FirebaseConstants.CHILD_PATIENT);
        String key = mRef.push().getKey();
        mRef.child(key).child("lastName").setValue("aguilar");
        long num = (long) (Math.random() * 99999999 + 10000000);
        mRef.child(key).child("dni").setValue(String.valueOf(num));
        mRef.child(key).child("age").setValue(32);
        mRef.child(key).child("status").setValue("activo");
        mRef.child(key).child("name").setValue("juan");
        mRef.child(key).child("origin").setValue("trujillo");
        int day = (int) (Math.random() * 30 + 1);
        int month = (int) (Math.random() * 12 + 1);
        int year = (int) (Math.random() * (2010 - 1970 + 1) + 1970);
        String[] datetime = new String[3];
        if (day < 10){
            datetime[0] = "0"+ day;
        } else {
            datetime[0] = String.valueOf(day);
        }
        if (month < 10){
            datetime[1] = "0"+ month;
        } else {
            datetime[1] = String.valueOf(month);
        }
        if (year < 10){
            datetime[2] = "0"+ year;
        } else {
            datetime[2] = String.valueOf(year);
        }

        mRef.child(key).child("birthdate").setValue(datetime[0] + "/" + datetime[1] + "/" + datetime[2]);
        int var = (int) (Math.random() * 12 + 0);
        mRef.child(key).child("photo_profile").setValue(FirebaseConstants.URL_PHOTO[var]);

        Toast.makeText(getApplicationContext(), "INSERT OK", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bSearch:
                if (etDni.getText() != null) {
                    verifyShowButtonOrList = true;

                    progressDialog.setMessage("Conectando...");
                    progressDialog.show();

                    dni = etDni.getText().toString();
                    bithDate = etBirthDate.getText().toString();

                    searchPatients(dni, bithDate);
                } else {
                    Toast.makeText(getApplicationContext(), R.string.nodata, Toast.LENGTH_SHORT).show();
                }
                break;
            //case R.id.bNewSearch:
            //    insertData();
            //    break;
            default:
                break;
        }
    }

    private void searchPatients(final String dni, final String birthDate) {
        try {
            Query query = mRef.orderByChild("dni").equalTo(dni);

            if (query == null) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), R.string.dniErr, Toast.LENGTH_LONG).show();
                layoutCardID.setVisibility(View.GONE);
            } else {
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        clearInputs();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {

                            if (child != null) {
                                Patient model = child.getValue(Patient.class);
                                if (backBirthDatePassword(model.getBirthdate()).equalsIgnoreCase(birthDate)) {
                                    layoutCardID.setVisibility(View.VISIBLE);

                                    final String postKey = child.getKey();
                                    final String postKeyBirth = model.getBirthdate();

                                    Glide.with(getApplicationContext())
                                            .load(model.getPhoto_profile())
                                            .crossFade()
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(imagePatient);
                                    tvNamePatient.setText(model.getName());
                                    tvLastNamePatient.setText(model.getLastName());
                                    tvAgePatient.setText(String.valueOf(model.getAge()));
                                    tvDniPatient.setText(model.getDni());
                                    tvOriginPatient.setText(model.getOrigin());

                                    progressDialog.dismiss();

                                    bRequestAppointment = (Button) findViewById(R.id.bRequestAppointment);
                                    bImagePatient = (ImageButton) findViewById(R.id.imagePatient);

                                    if (verifyShowButtonOrList){
                                        bRequestAppointment.setVisibility(View.VISIBLE);

                                        bImagePatient.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                goViewRequestAppointment(postKey,postKeyBirth);
                                            }
                                        });
                                        bRequestAppointment.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                goViewRequestAppointment(postKey,postKeyBirth);
                                            }
                                        });
                                    } else {
                                        bRequestAppointment.setVisibility(View.GONE);
                                        layoutPendingAppointment.setVisibility(View.VISIBLE);

                                        showDataPendingAppointment(postKey);
                                    }

                                } else {
                                    progressDialog.dismiss();
                                    layoutCardID.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(), R.string.birthErr, Toast.LENGTH_LONG).show();
                                }
                            } else {
                                progressDialog.dismiss();
                                layoutCardID.setVisibility(View.GONE);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        } catch (RuntimeException ex) {
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), R.string.dataErr, Toast.LENGTH_LONG).show();
            layoutCardID.setVisibility(View.GONE);
            ex.printStackTrace();
        }

    }

    private void showDataPendingAppointment(final String postKey) {
        mRefAppointment.keepSynced(true);
        mRefAppointment.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Appointment appointment = child.getValue(Appointment.class);
                    if (appointment.getPatient().equals(postKey)){
                        Log.i(TAG, "show entities data -> " + appointment.getSpecialty());
                        ArrayList<Appointment> list = new ArrayList<>();
                        list.add(appointment);
                        adapterAppointment.addDataList(list);
                    } else {
                        Log.i(TAG, "show all entities data -> " + appointment.getSpecialty());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void clearInputs() {
        etDni.setText("");
        etBirthDate.setText("");
        layoutCardID.setVisibility(View.GONE);
    }

    private String backBirthDatePassword(final String date) {
        String[] partDate = date.split("/");
        String password = partDate[0] + partDate[1] + partDate[2];
        return password;
    }

    private void goViewRequestAppointment(final String key, final String postKeyBirth){
        Intent intent = new Intent(getApplicationContext()  , AppointmentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(AppointmentActivity.EXTRA_POST_KEY, key);
        intent.putExtra(AppointmentActivity.EXTRA_POST_KEY_BIRTH_DATE, postKeyBirth);
        getApplication().startActivity(intent);
    }
}
