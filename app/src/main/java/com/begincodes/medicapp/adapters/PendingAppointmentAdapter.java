package com.begincodes.medicapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.begincodes.medicapp.R;
import com.begincodes.medicapp.models.Appointment;

import java.util.ArrayList;

/**
 * Created by manuelguarniz on 11/12/16.
 */
public class PendingAppointmentAdapter extends RecyclerView.Adapter<PendingAppointmentAdapter.ViewHolder> {
    private ArrayList<Appointment> arrayAppointment;
    private Context context;

    public PendingAppointmentAdapter(Context context) {
        this.context = context;
        this.arrayAppointment = new ArrayList<>();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_appointment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Appointment appointment = arrayAppointment.get(position);
        holder.specialties.setText(appointment.getSpecialty());
        holder.datePrograming.setText(appointment.getProgrammingDate());
        holder.hours.setText(appointment.getHours());
    }

    @Override
    public int getItemCount() {
        return arrayAppointment.size();
    }

    public void addDataList(ArrayList<Appointment> list) {
        arrayAppointment.addAll(list);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView specialties;
        public TextView datePrograming;
        public TextView hours;

        public ViewHolder(View itemView) {
            super(itemView);
            specialties = (TextView) itemView.findViewById(R.id.tvDetailSpecialties);
            datePrograming = (TextView) itemView.findViewById(R.id.tvDetailDatePrograming);
            hours = (TextView) itemView.findViewById(R.id.tvDetailHours);
        }
    }
}
