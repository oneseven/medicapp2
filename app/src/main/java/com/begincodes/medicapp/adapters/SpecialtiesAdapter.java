package com.begincodes.medicapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.begincodes.medicapp.R;

import java.util.List;

/**
 * Created by manuelguarniz on 09/12/16.
 */
public class SpecialtiesAdapter extends BaseAdapter {

    private List<String> list;
    private Activity activity;
    private LayoutInflater inflater;

    public SpecialtiesAdapter(List<String> list, Activity activity) {
        this.list = list;
        this.activity = activity;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.card_item_specialties, null);
        }
        TextView tvSpinnerSpecialties = (TextView) view.findViewById(R.id.tvSpinnerSpecialties);
        tvSpinnerSpecialties.setText(list.get(position));
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        LinearLayout layout = (LinearLayout) view;
        TextView tvSpinnerSpecialties = (TextView) layout.findViewById(R.id.tvSpinnerSpecialties);
        tvSpinnerSpecialties.setGravity(Gravity.LEFT);
        tvSpinnerSpecialties.setTextColor(Color.parseColor("#333639"));
        tvSpinnerSpecialties.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        return view;
    }
}
